<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Character;
use App\Form\CharacterType;
use FOS\RestBundle\Routing\ClassResourceInterface;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CharacterController extends AbstractController implements ClassResourceInterface
{
    /**
     * @var Serializer
     */
    private $serializer;

    private function setUpSerializer(): void
    {
        $this->serializer = SerializerBuilder::create()->build();
    }

    /**
     * Creates a 404 response with a potential custom error message.
     *
     * @param string $errorMessage
     *
     * @return JsonResponse
     */
    private function renderBadRequest(string $errorMessage = 'failure'): JsonResponse
    {
        return new JsonResponse($this->serializer->serialize(['msg' => $errorMessage], 'json'), 404, [], true);
    }

    /**
     * Get all characters.
     *
     * @return JsonResponse
     */
    public function cgetAction(): JsonResponse
    {
        $this->setUpSerializer();
        $characters = $this->getDoctrine()->getRepository(Character::class)->findAll();

        if (!$characters) {
            return $this->renderBadRequest('No characters found');
        }

        return new JsonResponse($this->serializer->serialize($characters, 'json'), 200, [], true);
    }

    /**
     * Get a single character.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function getAction(int $id): JsonResponse
    {
        $this->setUpSerializer();
        $character = $this->getDoctrine()->getRepository(Character::class)->find($id);

        if (!$character) {
            return $this->renderBadRequest('Character not found (id: '.$id.').');
        }

        return new JsonResponse($this->serializer->serialize($character, 'json'), 200, [], true);
    }

    /**
     * Create a new character.
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postAction(Request $request): JsonResponse
    {
        $this->setUpSerializer();
        $character = new Character();
        $form = $this->createForm(CharacterType::class, $character);
        $form->submit($request->request->all());

        if ($form->isSubmitted() && $form->isValid()) {
            $character = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($character);
            $entityManager->flush();

            return new JsonResponse(
                $this->serializer->serialize(['msg' => 'Character '.$form->get('name')->getData().' created'], 'json'),
                200,
                [],
                true
            );
        }

        return $this->renderBadRequest('Error occurred while sending the form. Check the data.');
    }

    /**
     * Edit a character
     *
     * @param Request $request
     * @param int $id
     *
     * @return JsonResponse
     */
    public function patchAction(Request $request, int $id): JsonResponse
    {
        $this->setUpSerializer();
        $entityManager = $this->getDoctrine()->getManager();
        $character = $entityManager->getRepository(Character::class)->find($id);

        if (!$character) {
            return $this->renderBadRequest('Character not found (id: '.$id.').');
        }

        $form = $this->createForm(CharacterType::class, $character);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $character = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($character);
            $entityManager->flush();

            return new JsonResponse(
                $this->serializer->serialize(['msg' => 'Character '.$form->get('name')->getData().' updated'], 'json'),
                200,
                [],
                true
            );
        }

        return $this->renderBadRequest('Error occurred while sending the form. Check the data.');
    }

    /**
     * Remove a character.
     *
     * @param int $id
     *
     * @return JsonResponse
     */
    public function deleteAction(int $id): JsonResponse
    {
        $this->setUpSerializer();
        $entityManager = $this->getDoctrine()->getManager();
        $character = $entityManager->getRepository(Character::class)->find($id);

        if (!$character) {
            return $this->renderBadRequest('Character not found (id: '.$id.').');
        }

        $entityManager->remove($character);
        $entityManager->flush();

        return new JsonResponse(
            $this->serializer->serialize(['msg' => 'Character with id:'.$id.' was successfully removed'], 'json'),
            200,
            [],
            true
        );
    }
}
